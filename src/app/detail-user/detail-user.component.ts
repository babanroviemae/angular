import { iUser } from './../models/contact';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.scss'],
})
export class DetailUserComponent implements OnInit {
  data: any;
  finalData!: iUser;
  constructor(private route:Router) {}

  ngOnInit(): void {
    this.data = localStorage.getItem('USER');
    this.finalData = JSON.parse(this.data);
  }

  onBack(){
    this.route.navigateByUrl('/contacts');
    localStorage.removeItem("USER");
  }
}
