import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],
})
export class ContactsComponent implements OnInit {
  currentUserId!: iEdit;
  contactEmit!: iSubmit;
  constructor(
    private userService: UserService
  ) {}

  ngOnInit(): void {}

  onEdit(event: any) {
    this.currentUserId = {
      action: 'edit',
      userId: event,
    };
  }

  onSubmit(event: iSubmit) {
    this.contactEmit = event;
    if (this.contactEmit.action === 'edit') {
      this.userService.put(
        this.contactEmit.data.value,
        this.currentUserId.userId
      );
    } else {
      this.userService.post(this.contactEmit.data.value);
    }
  }
}

export interface iAction {
  action: string;
}
export interface iEdit extends iAction {
  userId: string | number;
}

export interface iSubmit extends iAction {
  data: FormGroup;
}
