export interface iUser {
  id?: number;
  email: string;
  name: string;
  phone: string;
}


export function userDTO(
  id: number,
  email: string,
  name: string,
  phone: string
): iUser {
  return {
    id: id,
    email: email,
    name: name,
    phone: phone
  };
}
