import { FormGroup } from '@angular/forms';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { UserService } from './../services/user.service';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { iUser, userDTO } from '../models/contact';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss'],
})
export class ListUserComponent implements OnInit, OnChanges {
  displayedColumns = ['no', 'name', 'email', 'phone', 'action'];
  dataSource!: MatTableDataSource<iUser>;
  obs!: Observable<any>;
  Users: iUser[] = [];

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort!: MatSort;

  @Output() editAction = new EventEmitter();
  @Input() data: FormGroup | undefined;

  constructor(
    private userService: UserService,
    private route: Router
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
  }

  ngOnInit(): void {
    this.fetchUser();
  }

  fetchUpdate() {
    this.userService.getUpdatedListener().subscribe((resp) => {
      this.Users = resp.contacts;
      this.dataSource = new MatTableDataSource(this.Users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.obs = this.dataSource.connect();
    });
  }

  fetchUser() {
    this.userService.get();
    this.fetchUpdate();
  }

  onDetail(data: any) {
    if (data) {
      localStorage.setItem('USER', JSON.stringify(data));
      setTimeout(() => {
        this.route.navigateByUrl(`/detail/${data.id}`);
      }, 500);
    } else {
      alert('Data not found')
    }
  }

  onEdit(id: number | string) {
    this.editAction.emit(id);
  }

  onDelete(id: number | string) {
    this.userService.delete(id);
    this.fetchUpdate();
  }
}

